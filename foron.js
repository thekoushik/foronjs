(function(document,window,undefined){
    function DomTreeNodeScope(el){
        var self=this;
        this.currentNode=el;
        var currentWait=null;
        this.id=function(id){
            self.currentNode=document.getElementById(id);
            return self;
        };
        this.trigger=function(type){
            //var event = document.createEvent('Event');
            //event.initEvent(type, true, true);
            //self.currentNode.dispatchEvent(event);
            if(currentWait){
                setTimeout(function(){
                    self.currentNode.dispatchEvent(new Event(type,{"bubbles":true, "cancelable":true}));
                },currentWait);
                currentWait=null;
            }else
                self.currentNode.dispatchEvent(new Event(type,{"bubbles":true, "cancelable":true}));
            return self;
        };
        this.wait=function(mil){
            if(mil>0) currentWait=mil;
            return self;
        };
        this.msg=function(x){
            if(currentWait){
                setTimeout(function(){
                    alert(x);
                },currentWait);
                currentWait=null;
            }else
                alert(x);
            return self;
        };
        this.addClass=function(c){
            if(currentWait){
                setTimeout(function(){
                    self.currentNode.className+=" "+c;
                },currentWait);
                currentWait=null;
            }else
                self.currentNode.className+=" "+c;
            return self;
        };
        /*this.removeClass=function(c){
            self.currentNode.className=self.currentNode.className.replace(/hidden/, '');
            return self;
        };*/
        this.next=function(){
            if(self.currentNode.nextElementSibling!=null)
                self.currentNode=self.currentNode.nextElementSibling;
            return self;
        };
        this.prev=function(){
            if(self.currentNode.previousElementSibling!=null)
                self.currentNode=self.currentNode.previousElementSibling;
            return self;
        };
        this.parent=function(){
            self.currentNode=self.currentNode.parentElement;
            return self;
        };
    }
    function DomTreeNode(el){
        this.node=el;
        this.events={};
        this.children=[];
        this.scope=new DomTreeNodeScope(this.node);
    }
    var domTree={};
    var supportedAttributes=[
        {
            event:"click",
            name:"f-click"
        },
        {
            event:"contextmenu",
            name:"f-rclick"
        },
    ];
    var unSupportedTags=['SCRIPT'];
    function createNewEvent(__ev,__target,__code){
        __target.node.addEventListener(__ev,function(__e){
            /*
            function maskedEval(scr){
                // set up an object to serve as the context for the code being evaluated. 
                var mask = {};
                // mask global properties 
                for (p in this) mask[p] = undefined;
                // execute script in private context
                (new Function( "with(this) { " + scr + "}")).call(mask);
            }
            */
            (new Function( "with(this) { " + __code + "}")).call(__target.scope);
            //var json=JSON.parse(__code);
            //console.log(json);
        }, false);
    }
    function parseEvent(ev,evc,el,p){
        if(evc==null)
            return;
        
        p.events[ev]={code:evc};
        createNewEvent(ev,p,evc)
    }
    function scan(el){
        if(unSupportedTags.indexOf(el.tagName)>=0)
            return null;
        var p=new DomTreeNode(el);
        supportedAttributes.forEach(function(e){
            parseEvent(e.event,el.getAttribute(e.name),el,p);
        });
        var children=[];
        for(var i=0;i<el.children.length;i++){
            var c=scan(el.children[i]);
            if(c!=null) children.push(c);
        }
        p.children=children;
        return p;
    }
    function init(el){
        domTree=scan(el || document.body);
        //console.log(domTree);
    }
    //init();
    window.Foron={
        init:init
    };
})(document,window);